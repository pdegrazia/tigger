import itertools
import glob
import random
import pygame

size = width, height = 1200, 1000

full_screen = True

screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)

# info output
# print(pygame.display.Info())

pygame.joystick.init()
pygame.mixer.init()

try:
    joy = pygame.joystick.Joystick(0)
    joy.init()
except: # Too broad exception, need to import a better one from pygame
    print('Joystick not connected')


class Actor(pygame.sprite.Sprite):
    def __init__(self, filename):
        super(Actor, self).__init__()
        self.image = pygame.image.load("{}".format(filename)).convert_alpha()
        self.rect = self.image.get_rect()
        self.x = 0
        self.y = height/2
        self.is_animated = False
        self.up = True

    def enter(self):
        if self.x < width/2:
            self.x += 1
        else:
            self.is_animated = False
            self.play_random_sound()

        self.bounce()

    def exit(self):
        pass

    def draw(self):
        screen.blit(self.image, (self.x, self.y))

    def bounce(self):
        if self.up:
            self.y -= 5
        else:
            self.y += 5

        if self.y < 0:
            self.up = False
        elif self.y > 500:
            self.up = True

    def play_random_sound(self):
        song_file = random.choice(songs)
        #song = pygame.mixer.Sound(song_file)
        pygame.mixer.music.load(song_file)
        pygame.mixer.music.play()


actors = itertools.cycle([Actor(filename) for filename in glob.glob('images/*.png')])

current_actor = next(actors)

songs = [song_name for song_name in glob.glob('sounds/*.mp3')]


background = pygame.image.load('images/background_1600x900.jpg').convert_alpha()

while True:
    screen.fill([255, 255, 255])
    screen.blit(background, background.get_rect())

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                current_actor.x -= 10
            if event.key == pygame.K_RIGHT:
                current_actor.x += 10
            if event.key == pygame.K_SPACE:
                current_actor = next(actors)
                current_actor.is_animated = True
                current_actor.x = 0
            if event.key == pygame.K_ESCAPE:
                if screen.get_flags() & pygame.FULLSCREEN:
                    pygame.display.set_mode(size)

        if event.type == pygame.JOYBUTTONDOWN:
            print('Joy Pressed')
            current_actor = next(actors)
            current_actor.is_animated = True
            current_actor.x = 0

    if current_actor.is_animated:
        current_actor.enter()

    current_actor.draw()
    pygame.display.flip()
